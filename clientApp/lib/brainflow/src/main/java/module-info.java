module brainflow
{
    exports brainflow;

    requires java.sql;
    requires com.sun.jna;
    requires commons.math3;
    requires org.apache.commons.lang3;
    requires com.google.gson;
}
