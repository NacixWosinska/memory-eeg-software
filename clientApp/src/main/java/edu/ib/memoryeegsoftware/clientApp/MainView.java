package edu.ib.memoryeegsoftware.clientApp;

import edu.ib.memoryeegsoftware.clientApp.util.FXMLUtils;
import edu.ib.memoryeegsoftware.clientApp.util.LayoutUtils;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class MainView extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(FXMLUtils.loadFXML("main-view"));
        setUserAgentStylesheet(STYLESHEET_MODENA);
        Pane root = (Pane) scene.getRoot();

        stage.setScene(scene);
        stage.show();

        LayoutUtils layoutUtils = new LayoutUtils();
        layoutUtils.letterbox(scene, root);

        stage.setMaximized(false);
        stage.setMaximized(true);
        stage.setResizable(false);
    }


    public static void main(String[] args) {
        launch();
    }

}