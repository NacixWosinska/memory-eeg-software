package edu.ib.memoryeegsoftware.clientApp.controller;

import brainflow.BoardIds;
import brainflow.BoardShim;
import brainflow.BrainFlowError;
import com.fazecast.jSerialComm.SerialPort;
import edu.ib.memoryeegsoftware.clientApp.MainView;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertMessages;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertTitles;
import edu.ib.memoryeegsoftware.clientApp.util.AlertTemplate;
import edu.ib.memoryeegsoftware.clientApp.util.BoardUtils;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;

import java.awt.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static edu.ib.memoryeegsoftware.clientApp.util.BoardUtils.boardShim;

public class CheckConnectionController {

    @FXML
    public Button btnStopStream;

    @FXML
    private Button btnStartStreamCheckConnect;

    @FXML
    private LineChart<String, Double> chart1;

    @FXML
    private LineChart<String, Double> chart2;

    @FXML
    private LineChart<String, Double> chart3;

    @FXML
    private LineChart<String, Double> chart4;

    @FXML
    private LineChart<String, Double> chart5;

    @FXML
    private LineChart<String, Double> chart6;

    @FXML
    private LineChart<String, Double> chart7;

    @FXML
    private LineChart<String, Double> chart8;

    @FXML
    private ChoiceBox<String> chbChooseSerialPort;

    private final String RELATIVE_PATH_TO_CPLUS_LIBS = "\\clientApp\\src\\main\\resources\\lib";
    private final String RELATIVE_PATH_TO_CPLUS_LIBS_MAC = "//clientApp//src//main//resources//lib";

    private XYChart.Series<String, Double> series1;
    private XYChart.Series<String, Double> series2;
    private XYChart.Series<String, Double> series3;
    private XYChart.Series<String, Double> series4;
    private XYChart.Series<String, Double> series5;
    private XYChart.Series<String, Double> series6;
    private XYChart.Series<String, Double> series7;
    private XYChart.Series<String, Double> series8;
    private final BoardUtils boardUtils = new BoardUtils();

    private final int WINDOW_SIZE = 1000;
    private AlertTemplate alertTemplate = new AlertTemplate();
    private ScheduledExecutorService scheduledExecutorService;

    @FXML
    void btnStartStreamCheckConnectOnClick() throws BrainFlowError {

        String localDir = System.getProperty("user.dir");
        String pathToNativeLibs;
        String system_name = System.getProperty("os.name");
        if(system_name.contains("Windows")){
            pathToNativeLibs = localDir + RELATIVE_PATH_TO_CPLUS_LIBS;
        }else {
            pathToNativeLibs = localDir + RELATIVE_PATH_TO_CPLUS_LIBS_MAC;
        }
        System.setProperty("jna.library.path", pathToNativeLibs);

        if(chbChooseSerialPort.getValue() != null){
            ChoosePortModalController.serialPort = chbChooseSerialPort.getValue();

            if (boardShim == null || !boardShim.is_prepared()) {
                try {
                    boardShim = boardUtils.prepareDummyTestBoard();
                    //boardShim = boardUtils.prepareBoard(PatientDataFormController.serialPort);
                } catch (Exception error) {
                    alertTemplate.createAlert(Alert.AlertType.ERROR,
                            AlertMessages.BOARD_NOT_DETECTABLE_ALERT_MSG,
                            AlertMessages.BOARD_NOT_DETECTABLE_ALERT_HEADER,
                            AlertTitles.WARNING);
                }
            }
                boardShim.start_stream();

                setAxisLabels(chart1, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[0]);
                setAxisLabels(chart2, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[1]);
                setAxisLabels(chart3, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[2]);
                setAxisLabels(chart4, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[3]);
                setAxisLabels(chart5, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[4]);
                setAxisLabels(chart6, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[5]);
                setAxisLabels(chart7, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[6]);
                setAxisLabels(chart8, BoardShim.get_eeg_names(BoardIds.CYTON_BOARD)[7]);

                series1.getNode().setStyle("-fx-stroke: #3c6c70;");
                series2.getNode().setStyle("-fx-stroke: #3c6c70;");
                series3.getNode().setStyle("-fx-stroke: #3c6c70;");
                series4.getNode().setStyle("-fx-stroke: #3c6c70;");
                series5.getNode().setStyle("-fx-stroke: #3c6c70;");
                series6.getNode().setStyle("-fx-stroke: #3c6c70;");
                series7.getNode().setStyle("-fx-stroke: #3c6c70;");
                series8.getNode().setStyle("-fx-stroke: #3c6c70;");
                configureRealTimeUpdating();

            }
    }

    void setAxisLabels(LineChart<String, Double> chart, String channelName) {
        chart.setTitle(channelName);
        chart.getXAxis().setLabel("Timestamp [s]");
        chart.getYAxis().setLabel("Napięcie [uV]");
    }

    @FXML
    void btnStopStreamOnClick() throws BrainFlowError {
        if(scheduledExecutorService != null) {
            boardShim.stop_stream();
            scheduledExecutorService.shutdown();
        }

    }

    void configureRealTimeUpdating() {
            scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        scheduledExecutorService.scheduleAtFixedRate(() -> {
                Platform.runLater(() -> {
                    double[][] currVoltages;

                    try {
                        currVoltages = boardShim.get_current_board_data(1);
                        series1.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[1][0]));
                    series2.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[2][0]));
                    series3.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[3][0]));
                    series4.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[4][0]));
                    series5.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[5][0]));
                    series6.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[6][0]));
                    series7.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[7][0]));
                    series8.getData().add(new XYChart.Data<>(String.valueOf(currVoltages[22][0]), currVoltages[8][0]));

                    } catch (BrainFlowError e) {
                        e.printStackTrace();
                    }
                    if (series1.getData().size() > WINDOW_SIZE) {
                        series1.getData().remove(0);
                    series2.getData().remove(0);
                    series3.getData().remove(0);
                    series4.getData().remove(0);
                    series5.getData().remove(0);
                    series6.getData().remove(0);
                    series7.getData().remove(0);
                    series8.getData().remove(0);

                    }
                });
            }, 0, 16, TimeUnit.MILLISECONDS);
    }


    @FXML
    void initialize() {
        ObservableList<String> optionsSerialPorts = FXCollections.observableArrayList();
        SerialPort[] ports = SerialPort.getCommPorts();
        for (SerialPort port: ports)
            optionsSerialPorts.add(port.getSystemPortName());
        chbChooseSerialPort.setItems(optionsSerialPorts);

        Rectangle gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        double height = gd.getHeight();
        if(height >= 1000) {

            String  style= String.valueOf(MainView.class.getResource("css/btn-style.css"));
            btnStopStream.getStylesheets().add(style);
            btnStartStreamCheckConnect.getStylesheets().add(style);
        }
        series1 = new XYChart.Series<>();
        series2 = new XYChart.Series<>();
        series3 = new XYChart.Series<>();
        series4 = new XYChart.Series<>();
        series5 = new XYChart.Series<>();
        series6 = new XYChart.Series<>();
        series7 = new XYChart.Series<>();
        series8 = new XYChart.Series<>();
        chart1.setAnimated(false);
        chart2.setAnimated(false);
        chart3.setAnimated(false);
        chart4.setAnimated(false);
        chart5.setAnimated(false);
        chart6.setAnimated(false);
        chart7.setAnimated(false);
        chart8.setAnimated(false);
        chart1.getXAxis().setTickLabelsVisible(false);
        chart2.getXAxis().setTickLabelsVisible(false);
        chart3.getXAxis().setTickLabelsVisible(false);
        chart4.getXAxis().setTickLabelsVisible(false);
        chart5.getXAxis().setTickLabelsVisible(false);
        chart6.getXAxis().setTickLabelsVisible(false);
        chart7.getXAxis().setTickLabelsVisible(false);
        chart8.getXAxis().setTickLabelsVisible(false);
        chart1.setLegendVisible(false);
        chart2.setLegendVisible(false);
        chart3.setLegendVisible(false);
        chart4.setLegendVisible(false);
        chart5.setLegendVisible(false);
        chart6.setLegendVisible(false);
        chart7.setLegendVisible(false);
        chart8.setLegendVisible(false);
        chart1.setCreateSymbols(false);
        chart2.setCreateSymbols(false);
        chart3.setCreateSymbols(false);
        chart4.setCreateSymbols(false);
        chart5.setCreateSymbols(false);
        chart6.setCreateSymbols(false);
        chart7.setCreateSymbols(false);
        chart8.setCreateSymbols(false);

        chart1.getData().add(series1);
        chart2.getData().add(series2);
        chart3.getData().add(series3);
        chart4.getData().add(series4);
        chart5.getData().add(series5);
        chart6.getData().add(series6);
        chart7.getData().add(series7);
        chart8.getData().add(series8);


    }

 }
