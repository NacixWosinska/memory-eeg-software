package edu.ib.memoryeegsoftware.clientApp.controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import com.fazecast.jSerialComm.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ChoosePortModalController {

    @FXML
    private Button btnZatwierdzFormularz;

    @FXML
    private ChoiceBox<String> chbSerialPorts;

    protected static String serialPort;

    @FXML
    private Label labelRozwinListe;

    @FXML
    void btnZatwierdzFormularzOnClick() {
        if(chbSerialPorts.getValue() == null){
            labelRozwinListe.setText("Aby przejść dalej należy wybrać port.");
        }else{
            serialPort = chbSerialPorts.getValue();
            Stage stage = (Stage) chbSerialPorts.getScene().getWindow();
            stage.close();
        }
    }

    @FXML
    void chbSerialPortsOnMouseClicked() {
        labelRozwinListe.setVisible(false);
    }

    @FXML
    void initialize() {
        ObservableList<String> optionsSerialPorts = FXCollections.observableArrayList();
        SerialPort[] ports = SerialPort.getCommPorts();
        for (SerialPort port: ports)
            optionsSerialPorts.add(port.getSystemPortName());
        chbSerialPorts.setItems(optionsSerialPorts);

        assert btnZatwierdzFormularz != null : "fx:id=\"btnZatwierdzFormularz\" was not injected: check your FXML file 'choose-serial-port.fxml'.";
        assert chbSerialPorts != null : "fx:id=\"chbSerialPorts\" was not injected: check your FXML file 'choose-serial-port.fxml'.";
        assert labelRozwinListe != null : "fx:id=\"labelRozwinListe\" was not injected: check your FXML file 'choose-serial-port.fxml'.";
    }

}
