package edu.ib.memoryeegsoftware.clientApp.controller;

import brainflow.BrainFlowError;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertMessages;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertTitles;
import edu.ib.memoryeegsoftware.clientApp.util.AlertTemplate;
import edu.ib.memoryeegsoftware.clientApp.util.BoardUtils;
import edu.ib.memoryeegsoftware.clientApp.util.FXMLUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

import static edu.ib.memoryeegsoftware.clientApp.controller.ChoosePortModalController.serialPort;
import static edu.ib.memoryeegsoftware.clientApp.util.BoardUtils.boardShim;

public class CustomTestViewController {

    @FXML
    private Button btnStart;
    @FXML
    private Button btnStop;
    @FXML
    private ImageView imageStart;
    private final String RELATIVE_PATH_TO_CPLUS_LIBS = "\\clientApp\\src\\main\\resources\\lib";
    private final String RELATIVE_PATH_TO_CPLUS_LIBS_MAC = "//clientApp//src//main//resources//lib";
    private final BoardUtils boardUtils = new BoardUtils();

    private AlertTemplate alertTemplate = new AlertTemplate();
    @FXML
    void btnStartOnClick(ActionEvent event) throws BrainFlowError {
        btnStart.setVisible(false);
        imageStart.setVisible(false);
        String localDir = System.getProperty("user.dir");
        String pathToNativeLibs;
        String system_name = System.getProperty("os.name");
        if (system_name.contains("Windows")) {
            pathToNativeLibs = localDir + RELATIVE_PATH_TO_CPLUS_LIBS;
        } else {
            pathToNativeLibs = localDir + RELATIVE_PATH_TO_CPLUS_LIBS_MAC;
        }
        System.setProperty("jna.library.path", pathToNativeLibs);

        if (serialPort != null) {

            if (boardShim == null || !boardShim.is_prepared()) {
                try {
                    boardShim = boardUtils.prepareDummyTestBoard();
                    //boardShim = boardUtils.prepareBoard(PatientDataFormController.serialPort);
                } catch (Exception error) {
                    alertTemplate.createAlert(Alert.AlertType.ERROR,
                            AlertMessages.BOARD_NOT_DETECTABLE_ALERT_MSG,
                            AlertMessages.BOARD_NOT_DETECTABLE_ALERT_HEADER,
                            AlertTitles.WARNING);
                }
            }
            boardShim.start_stream();

        }
    }

    @FXML
    void btnStopOnClick() throws BrainFlowError {
        btnStart.setVisible(true);
        imageStart.setVisible(true);
        boardShim.stop_stream();
    }

    @FXML
        void initialize() throws IOException {
            Stage stageSerialPort = new Stage();
            Parent root = FXMLUtils.loadFXML("choose-serial-port");
            stageSerialPort.setScene(new Scene(root));
            stageSerialPort.initModality(Modality.APPLICATION_MODAL);
            stageSerialPort.initStyle(StageStyle.UNDECORATED);
            stageSerialPort.show();
        }

}
