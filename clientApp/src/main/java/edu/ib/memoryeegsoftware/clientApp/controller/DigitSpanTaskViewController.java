package edu.ib.memoryeegsoftware.clientApp.controller;

import brainflow.BrainFlowError;
import edu.ib.memoryeegsoftware.clientApp.MainView;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertMessages;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertTitles;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.InstructionMessages;
import edu.ib.memoryeegsoftware.clientApp.util.*;
import edu.ib.memoryeegsoftware.clientApp.entity.Marker;
import edu.ib.memoryeegsoftware.clientApp.entity.MarkerCode;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static edu.ib.memoryeegsoftware.clientApp.util.BoardUtils.boardShim;

public class DigitSpanTaskViewController {

    private final LinkedList<Integer> sequenceToRemember = new LinkedList<>();
    private final LinkedList<Integer> rememberedSequence = new LinkedList<>();
    private final BoardUtils boardUtils = new BoardUtils();
    @FXML
    private Button btnNumber0;
    @FXML
    private Button btnNumber1;
    @FXML
    private Button btnNumber2;
    @FXML
    private Button btnNumber3;
    @FXML
    private Button btnNumber4;
    @FXML
    private Button btnNumber5;
    @FXML
    private Button btnNumber6;
    @FXML
    private Button btnNumber7;
    @FXML
    private Button btnNumber8;
    @FXML
    private Button btnNumber9;
    @FXML
    private Button btnStart;
    @FXML
    private Button btnUndoNumber;
    @FXML
    private Button btnConfirmAnswer;
    @FXML
    private Label labelInstruction;
    @FXML
    private Label labelDigitsToRemember;
    @FXML
    private TextArea tfAnswer;
    @FXML
    private Label labelTestCounter;
    @FXML
    private ImageView imageStart;
    private int digitsCountPerTurn = 1;
    private String rememberedSequenceString = "";
    private int testCount = 1;
    private CalculatorUtils calculatorUtils;
    private ArrayList<Marker> markers;
    private final AlertTemplate alertTemplate = new AlertTemplate();
    private final ApiUtils apiUtils = new ApiUtils();
    private final String RELATIVE_PATH_TO_CPLUS_LIBS = "\\clientApp\\src\\main\\resources\\lib";
    private final String RELATIVE_PATH_TO_CPLUS_LIBS_MAC = "//clientApp//src//main//resources//lib";

    @FXML
    void btnUndoNumberOnClick() {
        rememberedSequence.clear();
        calculatorUtils.getCurrentDisplayedText(rememberedSequence);
        rememberedSequence.pollLast();
        rememberedSequenceString = "";
        for (Integer digit : rememberedSequence) {
            rememberedSequenceString += digit.toString();
        }
        tfAnswer.setText(rememberedSequenceString);
    }

    @FXML
    void btnStartOnClick() throws Exception {
        String localDir = System.getProperty("user.dir");
        String pathToNativeLibs;
        String system_name = System.getProperty("os.name");
        if(system_name.contains("Windows")){
            pathToNativeLibs = localDir + RELATIVE_PATH_TO_CPLUS_LIBS;
        }else {
            pathToNativeLibs = localDir + RELATIVE_PATH_TO_CPLUS_LIBS_MAC;
        }
        System.setProperty("jna.library.path", pathToNativeLibs);

        if (boardShim == null || !boardShim.is_prepared()) {
            /// MARKER - START OF A NEW TURN
            try {
                boardShim = boardUtils.prepareDummyTestBoard();
                //boardShim = boardUtils.prepareBoard(serialPort);
                boardUtils.startStream(boardShim);
            } catch (Exception error) {
                setToDisabled();
            alertTemplate.createAlert(Alert.AlertType.ERROR,
                    AlertMessages.BOARD_NOT_DETECTABLE_ALERT_MSG,
                    AlertMessages.BOARD_NOT_DETECTABLE_ALERT_HEADER,
                    AlertTitles.WARNING);
        }
        }

        startAndValidBoard();
    }

    private void startAndValidBoard() {
            if (digitsCountPerTurn == 1) {
                try {
                    testTimelineTurn();
                } catch (Exception streamAlreadyRunError) {
                    setToDisabled();
                    alertTemplate.createAlert(Alert.AlertType.ERROR,
                            AlertMessages.STREAM_ALREADY_RUN_ALERT_MSG,
                            AlertMessages.STREAM_ALREADY_RUN_ALERT_HEADER,
                            AlertTitles.WARNING);
                }
            }else {
                testTimelineTurn();
            }
    }

    private void testTimelineTurn() {
        Random random = new Random();
        rememberedSequence.clear();
        tfAnswer.clear();
        rememberedSequenceString = "";
        sequenceToRemember.clear();
        btnStart.setVisible(false);
        imageStart.setVisible(false);

        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(150), event1 -> {
            labelDigitsToRemember.setVisible(true);
            int randomDigit = random.nextInt(9 + 1 - 1) + 1;
            labelDigitsToRemember.setText(String.valueOf(randomDigit));
            try {
                markers.add(new Marker(boardShim.get_board_data_count(), MarkerCode.GET, true));
            } catch (BrainFlowError e) {
                throw new RuntimeException(e);
            }
            labelDigitsToRemember.setAlignment(Pos.CENTER);
            sequenceToRemember.add(randomDigit);
        }), new KeyFrame((Duration.millis(1200)), event2 -> labelDigitsToRemember.setVisible(false)));
        timeline.setCycleCount(digitsCountPerTurn);

        timeline.play();
        timeline.setOnFinished(event3 -> {
            /// BEGINNING OF RECREATING SEQUENCE IN MEMORY - USER IS TYPING
            labelDigitsToRemember.setVisible(false);

            setToEnabled();
            labelInstruction.setText(InstructionMessages.WRITE_REMEMBERED);
        });
    }


    @FXML
    void btnConfirmAnswerOnClick() throws BrainFlowError {
        rememberedSequence.clear();

        try {
            markers.add(new Marker(boardShim.get_board_data_count(), MarkerCode.RECREATE_END));
        } catch (BrainFlowError e) {
            e.printStackTrace();
        }
        for (char digitsTypedManually : tfAnswer.getText().toCharArray()) {
            rememberedSequence.add(Integer.parseInt(String.valueOf(digitsTypedManually)));
        }
        if (Objects.equals(rememberedSequence.toString(), sequenceToRemember.toString())) {
            btnStart.setVisible(true);
            imageStart.setVisible(true);
            setToDisabled();
            /// MARKER - END OF THE TURN - WAITING FOR USER TO START NEXT TURN

            markers.set(markers.size()-1, new Marker(markers.get(markers.size()-1).getIndex(), markers.get(markers.size()-1).getMarkerCode(), true));
            labelInstruction.setText(InstructionMessages.START_NEW_TURN);
            digitsCountPerTurn++;
        } else {
            /// MARKER - END OF THE TEST
            setToDisabled();

            if(rememberedSequence.size()<sequenceToRemember.size()){
            for(int j = rememberedSequence.size(); j < sequenceToRemember.size(); j++){
                int position = markers.size() - 1 - sequenceToRemember.size() + j;
                markers.set(position, new Marker(markers.get(position).getIndex(), markers.get(position).getMarkerCode(), false));
            }
                for(int i = 0; i < rememberedSequence.size(); i++){
                    if (!Objects.equals(sequenceToRemember.get(i), rememberedSequence.get(i))) {
                        int position = markers.size() - 1 - sequenceToRemember.size() + i;
                        markers.set(position, new Marker(markers.get(position).getIndex(), markers.get(position).getMarkerCode(), false));
                    }
                }
            }else if(rememberedSequence.size()>sequenceToRemember.size()){
                for(int j = sequenceToRemember.size(); j < rememberedSequence.size(); j++){
                    int position = markers.size() - 1 - rememberedSequence.size() + j;
                    markers.set(position, new Marker(markers.get(position).getIndex(), markers.get(position).getMarkerCode(), false));
                }

                for(int i = 0; i < sequenceToRemember.size(); i++){
                    if (!Objects.equals(sequenceToRemember.get(i), rememberedSequence.get(i))) {
                        int position = markers.size() - 1 - sequenceToRemember.size() + i;
                        markers.set(position, new Marker(markers.get(position).getIndex(), markers.get(position).getMarkerCode(), false));
                    }
                }
            }else{
                for(int i = 0; i < sequenceToRemember.size(); i++){
                    if (!Objects.equals(sequenceToRemember.get(i), rememberedSequence.get(i))) {
                        int position = markers.size() - 1 - sequenceToRemember.size() + i;
                        markers.set(position, new Marker(markers.get(position).getIndex(), markers.get(position).getMarkerCode(), false));
                    }
                }
            }

            markers.set(markers.size()-1, new Marker(markers.get(markers.size()-1).getIndex(), markers.get(markers.size()-1).getMarkerCode(), false));

            if (testCount == 12) {
                // MARKER - END OF THE SESSION
                class SendToDatabase extends Thread {
                    public void run() {
                        try {
                            saveMarkersToFile(saveDataToEDF());
                            boardUtils.stopStream(boardShim);
                            boardShim.release_session();
                            markers.clear();
                        } catch (BrainFlowError | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                SendToDatabase thread = new SendToDatabase();
                thread.start();

                alertTemplate.createAlert(Alert.AlertType.INFORMATION,
                        AlertMessages.SESSION_ENDED_ALERT_MSG,
                        AlertMessages.SESSION_ENDED_ALERT_HEADER,
                        AlertTitles.INFORMATION);
            } else {
                testCount++;
                alertTemplate.createAlert(Alert.AlertType.INFORMATION,
                        AlertMessages.TEST_ENDED_ALERT_MSG,
                        AlertMessages.TEST_ENDED_ALERT_HEADER,
                        AlertTitles.INFORMATION);
                setLabelInstructionAfterTurn();
                digitsCountPerTurn = 1;
                labelTestCounter.setText("Test: " + testCount + "/12");
                btnStart.setVisible(true);
                imageStart.setVisible(true);
            }
        }
    }

    private void saveMarkersToFile(String edfFileName) throws IOException {
        String fileName = "MARKERS_" + edfFileName;
        String pathToEDFFile;

        if (System.getProperty("os.name").contains("Windows")) {
            pathToEDFFile = "c:\\memoryeegsoftware-data\\markers\\";
        }else{
            pathToEDFFile = "c://memoryeegsoftware-data//markers//";
        }
        if (!Files.isDirectory(Path.of(pathToEDFFile))){
            Files.createDirectories(Path.of(pathToEDFFile));
        }
        FileWriter writer = new FileWriter(pathToEDFFile + fileName + ".csv");
        writer.write("index,type,isRemembered\n");

        for(Marker marker : markers) {
            writer.write(marker.getIndex() + "," + marker.getMarkerCode() + "," + marker.isRemembered());
            writer.write("\n");
        }
        writer.close();

        apiUtils.uploadMarkerFile(fileName + ".csv");
    }

    void setLabelInstructionAfterTurn() {
        if (digitsCountPerTurn == 1) {
            labelInstruction.setText("Nie udało się. Ciąg liczbowy do odtworzenia wyglądał następująco: " +
                    sequenceToRemember + ", natomiast wpisany: " + rememberedSequence);
        } else {
            labelInstruction.setText("Nie udało się. Ciąg liczbowy do odtworzenia wyglądał następująco: " +
                    sequenceToRemember + ", natomiast wpisany: " + rememberedSequence + "\nUdało się zapamiętać" +
                    " ciąg cyfrowy o długości maksymalnie " + (digitsCountPerTurn - 1) + ".");
        }
    }

    public String saveDataToEDF() throws BrainFlowError, IOException {
        double[][] dataFromTurn = boardShim.get_board_data();
        EdfConverterUtils edfConverterUtils = new EdfConverterUtils();
        edfConverterUtils.createEDF(dataFromTurn, 8);
        apiUtils.uploadFile(edfConverterUtils.getEdfFileName() + ".edf");

        return edfConverterUtils.getEdfFileName();
    }

    void setToDisabled() {
        btnConfirmAnswer.setDisable(true);
        tfAnswer.setDisable(true);
        btnNumber0.setDisable(true);
        btnNumber1.setDisable(true);
        btnNumber2.setDisable(true);
        btnNumber3.setDisable(true);
        btnNumber4.setDisable(true);
        btnNumber5.setDisable(true);
        btnNumber6.setDisable(true);
        btnNumber7.setDisable(true);
        btnNumber8.setDisable(true);
        btnNumber9.setDisable(true);
        btnUndoNumber.setDisable(true);
    }

    void setToEnabled() {
        btnConfirmAnswer.setDisable(false);
        tfAnswer.setDisable(false);
        btnNumber0.setDisable(false);
        btnNumber1.setDisable(false);
        btnNumber2.setDisable(false);
        btnNumber3.setDisable(false);
        btnNumber4.setDisable(false);
        btnNumber5.setDisable(false);
        btnNumber6.setDisable(false);
        btnNumber7.setDisable(false);
        btnNumber8.setDisable(false);
        btnNumber9.setDisable(false);
        btnUndoNumber.setDisable(false);
    }

    @FXML
    void initialize() throws IOException {
        setToDisabled();

        Rectangle gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        double height = gd.getHeight();

        if(height >= 1000) {
            labelInstruction.setStyle("-fx-font: "+"20px Consolas"+";");
            labelDigitsToRemember.setStyle("-fx-font: "+"100px Consolas"+";");
            labelTestCounter.setStyle("-fx-font: "+"50px Consolas"+";");
            tfAnswer.setStyle("-fx-font: "+"30px Consolas"+";");
            btnStart.setPrefSize(200, 200);
            String  style= String.valueOf(MainView.class.getResource("css/btn-style.css"));
            tfAnswer.setPrefWidth(700);
            btnStart.getStylesheets().add(style);
            btnConfirmAnswer.getStylesheets().add(style);
            btnConfirmAnswer.setPrefSize(700, 90);
        }

        markers = new ArrayList<>();
        calculatorUtils = new CalculatorUtils();
        calculatorUtils.setTfAnswer(tfAnswer);
        calculatorUtils.numericOnly();
        calculatorUtils.handleDigitsMouseEvents(btnNumber0, 0, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber1, 1, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber2, 2, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber3, 3, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber4, 4, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber5, 5, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber6, 6, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber7, 7, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber8, 8, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnNumber9, 9, rememberedSequence);
        calculatorUtils.handleDigitsMouseEvents(btnUndoNumber);
        labelDigitsToRemember.setVisible(false);
        Stage stageSerialPort = new Stage();
        Parent root = FXMLUtils.loadFXML("choose-serial-port");
        stageSerialPort.setScene(new Scene(root));
        stageSerialPort.initModality(Modality.APPLICATION_MODAL);
        stageSerialPort.initStyle(StageStyle.UNDECORATED);
        stageSerialPort.show();

    }
}
