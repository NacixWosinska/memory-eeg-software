package edu.ib.memoryeegsoftware.clientApp.controller;

import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertMessages;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertTitles;
import edu.ib.memoryeegsoftware.clientApp.util.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

import static edu.ib.memoryeegsoftware.clientApp.util.FXMLUtils.loadFXML;

public class MainViewController {

    @FXML
    private TextArea tfUsername;

    @FXML
    private PasswordField tfPassword;

    @FXML
    void btnContinueOnClick(ActionEvent event) throws IOException {
        String username = tfUsername.getText();
        String password = tfPassword.getText();

        if (!Objects.equals(username, "") && !Objects.equals(password, "")) {
            ApiUtils apiUtils = new ApiUtils();

            int responseCode = apiUtils.login(username, password);
            if (responseCode == 200) {
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Scene scene = new Scene(loadFXML("main-options-view"));
                stage.setScene(scene);
                Pane root = (Pane) scene.getRoot();
                LayoutUtils layoutUtils = new LayoutUtils();
                layoutUtils.letterbox(scene, root);
                stage.setMaximized(false);
                stage.setMaximized(true);
                stage.show();
            } else if (responseCode == 1) {
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Scene scene = new Scene(loadFXML("main-options-view_admin"));
                stage.setScene(scene);
                Pane root = (Pane) scene.getRoot();
                LayoutUtils layoutUtils = new LayoutUtils();
                layoutUtils.letterbox(scene, root);
                stage.setMaximized(false);
                stage.setMaximized(true);
                stage.show();
            } else {
                AlertTemplate alertTemplate2 = new AlertTemplate();
                alertTemplate2.createAlert(Alert.AlertType.ERROR,
                        AlertMessages.WRONG_AUTHORIZATION_MSG,
                        AlertMessages.WRONG_AUTHORIZATION_HEADER,
                        AlertTitles.WARNING);
            }
        }
    }
}
