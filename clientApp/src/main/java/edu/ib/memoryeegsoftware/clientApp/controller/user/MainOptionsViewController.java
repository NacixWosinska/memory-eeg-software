package edu.ib.memoryeegsoftware.clientApp.controller.user;

import brainflow.BrainFlowError;
import edu.ib.memoryeegsoftware.clientApp.MainView;
import edu.ib.memoryeegsoftware.clientApp.util.ApiUtils;
import edu.ib.memoryeegsoftware.clientApp.util.FXMLUtils;
import edu.ib.memoryeegsoftware.clientApp.util.LayoutUtils;
import edu.ib.memoryeegsoftware.clientApp.util.ResponsiveLayoutUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.awt.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static edu.ib.memoryeegsoftware.clientApp.util.BoardUtils.boardShim;


public class MainOptionsViewController {

    @FXML
    public VBox vbox_choose_test;
    @FXML
    private Label labelHello;
    @FXML
    private Label labelCheckOptions;

    @FXML
    private TabPane tabPaneTabs;

    private String startDate;

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }



    @FXML
    void btnCheckConnectionOnClick() throws IOException {
        setStartDate(getCurrentDate());
        Tab plusTab = new Tab("Połączenie " + startDate);
        tabPaneTabs.getTabs().add(plusTab);
        tabPaneTabs.getSelectionModel().selectLast();
        plusTab.setContent(FXMLUtils.loadFXML("check-connection-view"));
        labelHello.setVisible(false);
        labelCheckOptions.setVisible(false);
        plusTab.setOnCloseRequest(arg0 -> {
            try {
                if(boardShim != null && boardShim.is_prepared()) {
                    boardShim.stop_stream();
                    boardShim.release_session();
                }
            } catch (BrainFlowError e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void btLogoutOnClick() throws IOException {

    }

    @FXML
    void btnDownloadOnClick() throws IOException {
        ApiUtils apiUtils = new ApiUtils();
        apiUtils.downloadFiles();
    }
    @FXML
    void btnStartNewTestOnClick() {
        setStartDate(getCurrentDate());
        Tab plusTab = new Tab("Badanie " + startDate);
        tabPaneTabs.getTabs().add(plusTab);
        tabPaneTabs.getSelectionModel().selectLast();
        plusTab.setContent(vbox_choose_test);
        Pane root = (Pane) plusTab.getContent().getScene().getRoot();
        LayoutUtils layoutUtils = new LayoutUtils();
        layoutUtils.letterbox(plusTab.getContent().getScene(), root);
        labelHello.setVisible(false);
        labelCheckOptions.setVisible(false);
        plusTab.setOnCloseRequest(arg0 -> {
            try {
                if(boardShim != null && boardShim.is_prepared()) {
                    boardShim.stop_stream();
                    boardShim.release_session();
                }
            } catch (BrainFlowError e) {
                e.printStackTrace();
            }
        });

    }

    private String getCurrentDate(){
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    @FXML
    void initialize() {
        tabPaneTabs.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);
    }

    public void btnGetDigitSpanTaskOnClick() throws IOException {
        tabPaneTabs.getTabs().remove(tabPaneTabs.getTabs().size()-1);
        Tab plusTab = new Tab("Badanie " + startDate);
        tabPaneTabs.getTabs().add(plusTab);
        tabPaneTabs.getSelectionModel().selectLast();
        plusTab.setContent(FXMLUtils.loadFXML("digit-span-task-view"));

    }

    public void btnGetCustomTestOnClick() throws IOException {
        tabPaneTabs.getTabs().remove(tabPaneTabs.getTabs().size()-1);
        Tab plusTab = new Tab("Badanie " + startDate);
        tabPaneTabs.getTabs().add(plusTab);
        tabPaneTabs.getSelectionModel().selectLast();
        plusTab.setContent(FXMLUtils.loadFXML("custom-test-view"));
    }
}
