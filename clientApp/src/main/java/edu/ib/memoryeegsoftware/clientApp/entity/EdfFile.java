package edu.ib.memoryeegsoftware.clientApp.entity;

public class EdfFile {

    private String name;

    public EdfFile(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
