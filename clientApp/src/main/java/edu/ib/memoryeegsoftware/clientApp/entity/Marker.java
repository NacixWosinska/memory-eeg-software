package edu.ib.memoryeegsoftware.clientApp.entity;

public class Marker {

    private int index;
    private MarkerCode markerCode;
    private boolean isRemembered;

    public Marker(int index, MarkerCode markerCode) {
        this.index = index;
        this.markerCode = markerCode;
    }

    public Marker(int index, MarkerCode markerCode, boolean isRemembered) {
        this.index = index;
        this.markerCode = markerCode;
        this.isRemembered = isRemembered;
    }

    public boolean isRemembered() {
        return isRemembered;
    }

    public void setRemembered(boolean remembered) {
        isRemembered = remembered;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public MarkerCode getMarkerCode() {
        return markerCode;
    }

    public void setMarkerCode(MarkerCode markerCode) {
        this.markerCode = markerCode;
    }

    @Override
    public String toString() {
        return "Marker{" +
                "index=" + index +
                ", markerCode=" + markerCode +
                ", isRemembered=" + isRemembered +
                '}';
    }
}
