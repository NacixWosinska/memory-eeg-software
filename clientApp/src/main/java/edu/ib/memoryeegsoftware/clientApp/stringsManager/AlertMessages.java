package edu.ib.memoryeegsoftware.clientApp.stringsManager;

public class AlertMessages {

    public final static String STREAM_ALREADY_RUN_ALERT_MSG = "Nastąpiła próba rozpoczęcia sesji na płytce, na której aktualnie już są" +
            " zbierane dane. Najpierw zamknij zakładkę z wcześniej rozpoczętą sesją.";

    public final static String STREAM_ALREADY_RUN_ALERT_HEADER = "Zakończ poprzednią sesję";

    public final static String BOARD_NOT_DETECTABLE_ALERT_MSG = "Nie udało się połączyć z płytką. Sprawdź czy podłączona jest do" +
            " odpowiedniego portu.";

    public final static String BOARD_NOT_DETECTABLE_ALERT_HEADER = "Brak połączenia";

    public final static String SESSION_ENDED_ALERT_MSG = "Sesja zakończyła się.";

    public final static String SESSION_ENDED_ALERT_HEADER = "Koniec sesji";

    public final static String TEST_ENDED_ALERT_MSG = "Wpisany ciąg jest nieprawidłowy. Test zakończył się. Wystartuj nowy test, gdy będziesz" +
            " gotowy/a.";

    public final static String TEST_ENDED_ALERT_HEADER = "Koniec testu";
    public final static String NO_INTERNET_CONN_HEADER = "Brak połączenia internetowego";
    public final static String NO_INTERNET_CONN_MSG = "Aby zalogować się do aplikacji należy połączyć się z internetem.";

    public final static String WRONG_AUTHORIZATION_HEADER = "Błąd logowania";
    public final static String WRONG_AUTHORIZATION_MSG = "Niepoprawne dane logowania.";




}
