package edu.ib.memoryeegsoftware.clientApp.stringsManager;

public class InstructionMessages {

    public final static String WRITE_REMEMBERED = "Zapisz ciąg liczb, który zapamiętałeś.";

    public final static String START_NEW_TURN = "Rozmiar ciągu liczb do zapamiętania zwiększa się o 1. " +
            "Kliknij przycisk START, aby rozpocząć następną turę.";


}
