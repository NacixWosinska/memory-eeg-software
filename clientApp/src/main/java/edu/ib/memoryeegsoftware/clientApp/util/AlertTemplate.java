package edu.ib.memoryeegsoftware.clientApp.util;

import javafx.scene.control.Alert;

public class AlertTemplate {

    public void createAlert(Alert.AlertType alertType, String message, String header, String title) {
        Alert streamAlreadyRunAlert = new Alert(alertType,
                message);
        streamAlreadyRunAlert.setHeaderText(header);
        streamAlreadyRunAlert.setTitle(title);
        streamAlreadyRunAlert.showAndWait();
    }

}
