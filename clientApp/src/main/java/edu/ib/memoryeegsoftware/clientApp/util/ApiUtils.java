package edu.ib.memoryeegsoftware.clientApp.util;

import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertMessages;
import edu.ib.memoryeegsoftware.clientApp.stringsManager.AlertTitles;
import javafx.scene.control.Alert;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ApiUtils {

    public int uploadFile(String edfFileName) throws IOException {
        URL url = new URL("http://localhost:8080/api/testdataEDF?edfFileName=" + edfFileName);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        return con.getResponseCode();
    }

    public int uploadMarkerFile(String markerFileName) throws IOException {
        URL url = new URL("http://localhost:8080/api/testdataMarkers?markerFileName=" + markerFileName);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        return con.getResponseCode();
    }

    public int login(String username, String password) throws MalformedURLException {
        URL url = new URL("http://localhost:8080/login?username=" + username +
                "&password=" + password);
        try {
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            return con.getResponseCode();
        }catch(IOException e) {
            return 1;
        }
    }

    public int downloadFiles() throws IOException {

        URL obj = new URL("http://localhost:8080/api/testdata/all");
        try {
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            con.setConnectTimeout(3000);
            return con.getResponseCode();
        } catch (Exception e) {
            AlertTemplate alertTemplate = new AlertTemplate();
            alertTemplate.createAlert(Alert.AlertType.WARNING,
                    AlertMessages.NO_INTERNET_CONN_MSG,
                    AlertMessages.NO_INTERNET_CONN_HEADER,
                    AlertTitles.WARNING);
            return 0;
        }
    }

}
