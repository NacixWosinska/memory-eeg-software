package edu.ib.memoryeegsoftware.clientApp.util;

import brainflow.*;

import java.io.IOException;
import java.util.Arrays;

public class BoardUtils {

    public static BoardShim boardShim;

    public BoardShim prepareBoard(String serialPort) throws BrainFlowError, ReflectiveOperationException, IOException {
        BoardShim.enable_board_logger();
        BrainFlowInputParams params = new BrainFlowInputParams();
        params.serial_port = serialPort;
        params.master_board = 0;
        BoardIds boardId = BoardIds.CYTON_BOARD;
        boardShim = new BoardShim (boardId, params);
        System.out.println(boardShim.get_board_id());
        boardShim.prepare_session();

        return boardShim;
    }

    public BoardShim prepareDummyTestBoard() throws BrainFlowError, ReflectiveOperationException, IOException {
        BoardShim.enable_board_logger();
        BoardIds boardId = BoardIds.SYNTHETIC_BOARD;
        BrainFlowInputParams params = new BrainFlowInputParams();
        boardShim = new BoardShim(boardId, params);
        boardShim.prepare_session();
        return boardShim;
    }

    public void stopStream(BoardShim boardShim) throws BrainFlowError {
        boardShim.stop_stream();
    }

    public void startStream(BoardShim boardShim) throws Exception {
        boardShim.start_stream(450000);
        BoardShim.log_message(LogLevels.LEVEL_INFO.get_code(), "Start sleeping in the main thread");
    }

}
