package edu.ib.memoryeegsoftware.clientApp.util;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

import java.util.LinkedList;

public class CalculatorUtils {
    private TextArea tfAnswer;
    private String rememberedSequenceString = "";

    public void setTfAnswer(TextArea tfAnswer) {
        this.tfAnswer = tfAnswer;
    }

    public void numericOnly() {
        tfAnswer.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                tfAnswer.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    public void handleDigitsMouseEvents(Button pressedBtn) {

        EventHandler<MouseEvent> handlerPressed = event -> {
            Button numberButton = (Button) event.getTarget();
            numberButton.setStyle("-fx-background-color: #9db3aa");
        };

        EventHandler<MouseEvent> handlerReleased = event -> {
            Button numberButton = (Button) event.getTarget();
            numberButton.setStyle("-fx-background-color: #e1e9e6");
        };
        setBtnsHandler(handlerPressed, handlerReleased, pressedBtn);
    }

    public void handleDigitsMouseEvents(Button pressedBtn, int digit, LinkedList<Integer> rememberedSequence) {

        EventHandler<MouseEvent> handlerPressed = event -> {
            Button numberButton = (Button) event.getTarget();
            numberButton.setStyle("-fx-background-color: #9db3aa");

            getCurrentDisplayedText(rememberedSequence);

            if(event.getSource()==pressedBtn)
            {
                rememberedSequence.add(digit);
            }
            setCurrentTextString(rememberedSequence);

        };

        EventHandler<MouseEvent> handlerReleased = event -> {
            Button numberButton = (Button) event.getTarget();
            numberButton.setStyle("-fx-background-color: #e1e9e6");
        };
        setBtnsHandler(handlerPressed, handlerReleased, pressedBtn);
    }

    private void setBtnsHandler(EventHandler<MouseEvent> handlerPressed,
                                EventHandler<MouseEvent> handlerReleased, Button btn) {
        btn.setOnMousePressed(handlerPressed);
        btn.setOnMouseReleased(handlerReleased);
    }

    private void setCurrentTextString(LinkedList<Integer> rememberedSequence) {
        rememberedSequenceString = "";
        for (Integer digit : rememberedSequence) {
            rememberedSequenceString += digit.toString();
        }
        tfAnswer.setText(rememberedSequenceString);
    }

    public void getCurrentDisplayedText(LinkedList<Integer> rememberedSequence) {
        rememberedSequence.clear();
        for (char digitsTypedManually : tfAnswer.getText().toCharArray()) {
            rememberedSequence.add(Integer.parseInt(String.valueOf(digitsTypedManually)));
        }
    }

}
