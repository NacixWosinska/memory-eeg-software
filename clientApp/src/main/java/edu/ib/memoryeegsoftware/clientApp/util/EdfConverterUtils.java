package edu.ib.memoryeegsoftware.clientApp.util;

import brainflow.BoardIds;
import brainflow.BoardShim;
import brainflow.BrainFlowError;
import edflib.EDFException;
import edflib.EDFwriter;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

public class EdfConverterUtils {

    private int chan;
    private EDFwriter edfWriter;
    private int err;
    private String edfFileName;

    public String createEDFFileName() {
        return new Date().toInstant().toString()
                .replaceAll(":", "_")
                .replaceAll("-", "_");
    }


    public String getEdfFileName() {
        return edfFileName;
    }

    public void createEDF(double[][] records, int eegSignals) throws BrainFlowError {
        String[] eegNames = BoardShim.get_eeg_names(BoardIds.CYTON_BOARD);
        try {
            File theDir = new File("c:\\memoryeegsoftware-data\\edf_files");
            if (!theDir.exists()){
                theDir.mkdirs();
            }
            edfFileName = createEDFFileName();

            edfWriter = new EDFwriter("c:\\memoryeegsoftware-data\\edf_files\\" + edfFileName + ".edf", EDFwriter.EDFLIB_FILETYPE_EDFPLUS, eegSignals);
        } catch (IOException | EDFException e) {
            System.out.print("An error occured while opening the file: ");
            System.out.print(e.getMessage());
            return;
        }
        for (chan = 0; chan <= 7; chan++) {
            edfWriter.setPhysicalMaximum(chan, Arrays.stream(records[chan+1]).max().getAsDouble());
            edfWriter.setPhysicalMinimum(chan, Arrays.stream(records[chan+1]).min().getAsDouble());
            edfWriter.setDigitalMaximum(chan, 32767);
            edfWriter.setDigitalMinimum(chan, -32768);
            edfWriter.setPhysicalDimension(chan, "uV");
            edfWriter.setSampleFrequency(chan, records[chan+1].length);
            edfWriter.setSignalLabel(chan, eegNames[chan]);

        }

        for (chan = 0; chan <= 7; chan++) {
            try {
                err = edfWriter.writePhysicalSamples(records[chan+1]);
                if (err != 0) {
                    System.out.printf("writePhysicalSamples() returned error: %d\n", err);
                    return;
                }
            } catch (IOException e) {
                System.out.print("An error occured while closing the file: ");
                System.out.print(e.getMessage());
            }
        }

        try {
            edfWriter.close();
        } catch (IOException | EDFException e) {
            System.out.print("An error occured while closing the file: ");
            System.out.print(e.getMessage());
        }
    }
}
