package edu.ib.memoryeegsoftware.clientApp.util;

import edu.ib.memoryeegsoftware.clientApp.MainView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import java.io.IOException;
import java.net.URL;

public class FXMLUtils {


    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainView.class.getResource( fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static Parent loadFXML(URL location) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        return fxmlLoader.load();
    }


}
