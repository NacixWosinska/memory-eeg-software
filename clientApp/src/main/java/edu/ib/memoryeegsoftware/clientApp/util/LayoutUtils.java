package edu.ib.memoryeegsoftware.clientApp.util;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;

import java.awt.*;

public class LayoutUtils {

    public void letterbox(final Scene scene, final Pane contentPane) {
        final double initWidth  = scene.getWidth();
        final double initHeight = scene.getHeight();
        final double ratio      = initWidth / initHeight;
        ResponsiveLayoutUtils sizeListener = new ResponsiveLayoutUtils(scene, ratio, initHeight, initWidth, contentPane);
        scene.widthProperty().addListener(sizeListener);
        scene.heightProperty().addListener(sizeListener);
    }

}
