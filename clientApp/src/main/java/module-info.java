module edu.ib.memoryeegsoftware.clientApp {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires brainflow;
    requires com.fazecast.jSerialComm;
    requires edflib;
    requires com.fasterxml.jackson.databind;
    requires java.desktop;
    opens edu.ib.memoryeegsoftware.clientApp to javafx.fxml;
    exports edu.ib.memoryeegsoftware.clientApp;
    exports edu.ib.memoryeegsoftware.clientApp.util;
    opens edu.ib.memoryeegsoftware.clientApp.util to javafx.fxml;
    exports edu.ib.memoryeegsoftware.clientApp.controller;
    opens edu.ib.memoryeegsoftware.clientApp.controller to javafx.fxml;
    exports edu.ib.memoryeegsoftware.clientApp.entity;
    opens edu.ib.memoryeegsoftware.clientApp.entity to javafx.fxml;
    exports edu.ib.memoryeegsoftware.clientApp.controller.admin;
    opens edu.ib.memoryeegsoftware.clientApp.controller.admin to javafx.fxml;
    exports edu.ib.memoryeegsoftware.clientApp.controller.user;
    opens edu.ib.memoryeegsoftware.clientApp.controller.user to javafx.fxml;

}