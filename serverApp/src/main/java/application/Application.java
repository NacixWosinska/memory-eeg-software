package application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import repository.FirebaseAdminInit;

import java.io.IOException;

@ComponentScan(basePackages= {"controller"})
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
        FirebaseAdminInit firebaseAdminInit = new FirebaseAdminInit();
        firebaseAdminInit.init();
    }
}