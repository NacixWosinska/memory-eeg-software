package controller;

import com.google.api.client.json.webtoken.JsonWebToken;
import com.google.cloud.firestore.QuerySnapshot;
import lombok.Getter;
import lombok.Setter;
import model.Role;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import service.LoginService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;


@Getter
@Setter
@RestController
@ComponentScan(basePackages= {"service"})
public class LoginController {

    @Autowired
    public final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestParam("username") String username,
                                                @RequestParam("password") String password,
                                                       HttpServletResponse response) throws ExecutionException, InterruptedException {
        User loginCredentials = new User(username, password, Role.ADMIN.toString());
        Object searchResponse;
        searchResponse = loginService.getUser(loginCredentials);
        if(searchResponse != null){
                String sessionToken = RequestContextHolder.currentRequestAttributes().getSessionId();

                Cookie sessionTokenCookie = new Cookie("tokenAuth", sessionToken);
                response.addCookie(sessionTokenCookie);
                if(searchResponse == "Admin"){
                    return new ResponseEntity<>("Admin logged in", HttpStatus.OK);
                }
                return new ResponseEntity<>("Success!", HttpStatus.OK);
            }

        return new ResponseEntity<>("Login failed.", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/logout")
    public ResponseEntity<String> logout(HttpServletRequest request, HttpServletResponse response) {
            Cookie[] cookies = request.getCookies();
            String sessionToken = RequestContextHolder.currentRequestAttributes().getSessionId();
            int clear = 0;
            if(cookies != null) {
                List<Cookie> cookieList = Arrays.stream(cookies).toList();

                for (Cookie cookie : cookieList) {
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                    if (Objects.equals(cookie.getName(), "tokenAuth")) {
                        if(Objects.equals(cookie.getValue(), sessionToken)) {
                            HttpSession session = request.getSession(false);
                            clear = 1;
                            session.invalidate();
                        }
                    }
                }
                if (clear == 1)
                    return new ResponseEntity<>("Success!", HttpStatus.OK);
            }
        return new ResponseEntity<>("Logout failed.", HttpStatus.UNAUTHORIZED);
    }

}