package controller;


import model.EdfFile;
import model.EdfFiles;
import model.MarkerFiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.StorageDataService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

@RestController
public class StorageDataController {


    @Autowired
    public StorageDataService storageDataService = new StorageDataService();

    @GetMapping("/api/testdata/all")
    public boolean downloadAllData(HttpServletRequest request) throws ExecutionException, InterruptedException {
        return storageDataService.downloadAllFiles(request);
    }

    @PostMapping("/api/testdataEDF")
    public boolean uploadEDFFile(@RequestParam("edfFileName") String edfFileName, HttpServletRequest request) throws IOException, ExecutionException, InterruptedException {
        return storageDataService.uploadEdfFiles(edfFileName, request);
    }

    @PostMapping("/api/testdataMarkers")
    public boolean uploadMarkerFile(@RequestParam("markerFileName") String markerFileName, HttpServletRequest request) throws IOException, ExecutionException, InterruptedException {
        return storageDataService.uploadMarkerFiles(markerFileName, request);
    }
}
