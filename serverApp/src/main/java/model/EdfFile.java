package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EdfFile {

    private String name;

    public EdfFile(String name) {
        this.name = name;
    }
}
