package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarkerFile {

    private String name;

    public MarkerFile(String name) {
        this.name = name;
    }
}
