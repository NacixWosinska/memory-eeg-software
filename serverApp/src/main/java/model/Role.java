package model;

public enum Role {
    READ_ONLY, ADMIN
}
