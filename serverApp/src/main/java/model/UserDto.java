package model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.crypto.password.PasswordEncoder;
import security.PasswordEncoderConfig;

@Getter
@Setter
public class UserDto {
    private String username;
    private String hashPassword;
    private String role;
    public User user;

    public UserDto(User user) {
        this.username = user.getUsername();
        this.role = user.getRole();
        this.user = user;
        this.hashPassword = getEncodedPassword();
    }

    public String getEncodedPassword() {
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        String password = user.getPassword();
        PasswordEncoder passwordEncoder = passwordEncoderConfig.passwordEncoder();
        hashPassword = passwordEncoder.encode(password);
        return hashPassword;
    }

}
