package repository;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import lombok.Getter;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;


@Getter
@Setter
public class FirebaseAdminInit {

    public static FirebaseApp firebaseApp;
    public static final String KEY_FILENAME = "mes-server-admin-key.json";
    public static final String STORAGE_FILENAME = "storage_string.txt";
    public static final String PATH_TO_JSON_DIR = "\\serverApp\\src\\main\\java\\repository\\";
    public void init() throws IOException {

        String localDir = System.getProperty("user.dir");
        String path = localDir + PATH_TO_JSON_DIR;

        FileInputStream serviceAccount =
                new FileInputStream(path + KEY_FILENAME);
        String storageName = Files.readString
                (Path.of(path + STORAGE_FILENAME), StandardCharsets.UTF_8);

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setProjectId("memory-eeg-software-server")
                .setStorageBucket(storageName)
                .build();

        firebaseApp = FirebaseApp.initializeApp(options);

    }
}
