package service;

import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import security.PasswordEncoderConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

@Service
public class LoginService {

    private String currentUsername;
    private final String ADMIN_USERNAME = "NataliaW";

    @Autowired
    public LoginService() {
    }

    public Object getUser(User user) throws ExecutionException, InterruptedException {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference users =
                firestore.collection("users");
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        List<QueryDocumentSnapshot> documents = users.get().get().getDocuments();
        for (DocumentSnapshot document : documents) {
            User userFromQuery = document.toObject(User.class);
            if (user.getUsername().matches(userFromQuery.getUsername()) && passwordEncoderConfig.passwordEncoder()
                    .matches(user.getPassword(), userFromQuery.getPassword())) {

                currentUsername = user.getUsername();

                if(Objects.equals(currentUsername, "NataliaW")){
                    return "Admin";
                }

                return documents;
            }
        }
        return null;
    }

    public String getCurrentUserName() {
        if (currentUsername != null) return currentUsername;
        else return "No user is currently logged in";
    }


    public boolean isLoggedIn(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String sessionToken = RequestContextHolder.currentRequestAttributes().getSessionId();
        if (cookies == null) {
            return false;
        } else {
            List<Cookie> cookieList = Arrays.stream(cookies).toList();
            for (Cookie cookie : cookieList) {
                if (Objects.equals(cookie.getName(), "tokenAuth") &&
                        Objects.equals(cookie.getValue(), sessionToken)) {
                    return true;
                }
            }
        }
        return false;
    }

}
