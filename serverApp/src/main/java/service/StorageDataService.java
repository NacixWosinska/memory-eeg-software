package service;

import com.google.api.core.ApiFuture;
import com.google.api.gax.paging.Page;
import com.google.cloud.firestore.*;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.cloud.StorageClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class StorageDataService {

    private final String PATH_TO_EDF_FILES_WIN = "c:\\memoryeegsoftware-data\\edf_files\\";
    private final String PATH_TO_EDF_FILES_MAC = "c://memoryeegsoftware-data//edf_files//";
    private final String PATH_TO_MARKER_FILES_WIN = "c:\\memoryeegsoftware-data\\markers\\";
    private final String PATH_TO_MARKER_FILES_MAC = "c://memoryeegsoftware-data//markers//";
    @Autowired
    public LoginService loginService = new LoginService();
    private String PATH_EDF_FINAL;
    private String PATH_MARKERS_FINAL;

    public void setPaths() {
        String system_name = System.getProperty("os.name");
        if (system_name.contains("Windows")) {
            PATH_EDF_FINAL = PATH_TO_EDF_FILES_WIN;
            PATH_MARKERS_FINAL = PATH_TO_MARKER_FILES_WIN;
        } else {
            PATH_EDF_FINAL = PATH_TO_EDF_FILES_MAC;
            PATH_MARKERS_FINAL = PATH_TO_MARKER_FILES_MAC;
        }
    }

    public boolean downloadAllFiles(HttpServletRequest request) throws ExecutionException, InterruptedException {
        setPaths();

        Bucket bucket = StorageClient.getInstance().bucket();
        Page<Blob> blobs = bucket.list(Storage.BlobListOption.currentDirectory());
        if (!Files.exists(Path.of(PATH_EDF_FINAL))) {
            new File(PATH_EDF_FINAL).mkdirs();
        }
        if (!Files.exists(Path.of(PATH_MARKERS_FINAL))) {
            new File(PATH_MARKERS_FINAL).mkdirs();
        }
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference edfFileNames =
                firestore.collection("users").document(loginService.getCurrentUserName()).collection("fileNames");
        List<QueryDocumentSnapshot> documents = edfFileNames.get().get().getDocuments();
        ArrayList<String> finalEdfNamesFromDatabase = new ArrayList<>();
        for (DocumentSnapshot document : documents) {
            for (int i = 0; i < document.getData().size(); i++) {
                finalEdfNamesFromDatabase.add(document.get(String.valueOf(i), String.class));

            }
        }
        AtomicInteger i = new AtomicInteger();
        blobs.getValues().forEach(blob -> {
            if (finalEdfNamesFromDatabase.get(i.get()).contains("MARKERS")) {
                blob.downloadTo(Paths.get(PATH_MARKERS_FINAL + finalEdfNamesFromDatabase.get(i.get())));
            } else {
                blob.downloadTo(Paths.get(PATH_EDF_FINAL + finalEdfNamesFromDatabase.get(i.get())));
            }
            i.getAndIncrement();
        });
        return true;
    }

    public boolean uploadEdfFiles(String edfFileName, HttpServletRequest request) throws IOException, ExecutionException, InterruptedException {
        setPaths();
        Bucket bucket = StorageClient.getInstance().bucket();
        Storage storage = StorageClient.getInstance().bucket().getStorage();

        if (!Files.exists(Path.of(PATH_EDF_FINAL))) {
            new File(PATH_EDF_FINAL).mkdirs();
        }

        BlobId blobIdEdfFile = BlobId.of(bucket.getName(), edfFileName);
        BlobInfo blobInfoEdfFile = BlobInfo.newBuilder(blobIdEdfFile).build();
        storage.create(blobInfoEdfFile, Files.readAllBytes(Paths.get(PATH_EDF_FINAL + edfFileName)));


        Firestore firestore = FirestoreClient.getFirestore();
        ApiFuture<DocumentSnapshot> edfFileNames =
                firestore.collection("users").document(loginService.getCurrentUserName()).collection("fileNames").document(edfFileName.substring(0, 10)).get();
        Map<String, Object> docData = new HashMap<>();
        if (edfFileNames.get().exists()) {
            docData.put(String.valueOf(edfFileNames.get().getData().size()), edfFileName);
            ApiFuture<WriteResult> future = firestore.collection("users")
                    .document(loginService.getCurrentUserName()).collection("fileNames").document(edfFileName.substring(0, 10))
                    .update(docData);
            System.out.println("Update time : " + future.get().getUpdateTime());
        } else {
            docData.put(String.valueOf(0), edfFileName);
            ApiFuture<WriteResult> future = firestore.collection("users")
                    .document(loginService.getCurrentUserName()).collection("fileNames").document(edfFileName.substring(0, 10))
                    .set(docData);
            System.out.println("Update time : " + future.get().getUpdateTime());

        }
        return true;

    }

    public boolean uploadMarkerFiles(String markerFileName, HttpServletRequest request) throws IOException, ExecutionException, InterruptedException {
        setPaths();
        Bucket bucket = StorageClient.getInstance().bucket();
        Storage storage = StorageClient.getInstance().bucket().getStorage();

        if (!Files.exists(Path.of(PATH_MARKERS_FINAL))) {
            new File(PATH_MARKERS_FINAL).mkdirs();
        }

        BlobId blobIdMarkerFile = BlobId.of(bucket.getName(), markerFileName);
        BlobInfo blobInfoMarkerFile = BlobInfo.newBuilder(blobIdMarkerFile).build();
        storage.create(blobInfoMarkerFile, Files.readAllBytes(Paths.get(PATH_MARKERS_FINAL + markerFileName)));

        Firestore firestore = FirestoreClient.getFirestore();
        ApiFuture<DocumentSnapshot> fileNamesFromDatabase =
                firestore.collection("users").document(loginService.getCurrentUserName()).collection("fileNames").document(markerFileName.substring(8, 18)).get();
        Map<String, Object> docData = new HashMap<>();

        if (fileNamesFromDatabase.get().exists()) {
            docData.put(String.valueOf(fileNamesFromDatabase.get().getData().size()), markerFileName);
            ApiFuture<WriteResult> future = firestore.collection("users")
                    .document(loginService.getCurrentUserName()).collection("fileNames").document(markerFileName.substring(8, 18))
                    .update(docData);
            System.out.println("Update time : " + future.get().getUpdateTime());

        } else {
            docData.put(String.valueOf(0), markerFileName);
            ApiFuture<WriteResult> future = firestore.collection("users")
                    .document(loginService.getCurrentUserName()).collection("fileNames").document(markerFileName.substring(8, 18))
                    .set(docData);
            System.out.println("Update time : " + future.get().getUpdateTime());

        }
        return true;
    }
}
